$(function(){
			var obj = $('.area_img .slider').bxSlider({ // 自動再生
					auto: true,
					infiniteLoop: true,
					responsive: false,
					speed: 3000,
					mode: 'fade',
					controls: false,
					slideWidth: 1200, // 画像のサイズ
					pager: false,
					pause: 4000,
					onSlideAfter: function() { // 自動再生
						obj.startAuto();
				}
			});
			 var obj = $('.area_campus .slider').bxSlider({ // 自動再生
					auto: true,
					infiniteLoop: true,
					responsive: true,
					speed: 3000,
					controls: false,
					displaySlideQty: 1,
					pager: false,
					slideWidth: 250, // 画像のサイズ
					minSlides: 5,
					maxSlides: 5,
					moveSlides: 1,
					pause: 1500,
					onSlideAfter: function() { // 自動再生
						obj.startAuto();
				}
			});


		// 画像ホバー変換
		$('a img').hover(function(){
			$(this).attr('src', $(this).attr('src').replace('_off.', '_on.'));
				}, function(){
					if (!$(this).hasClass('currentPage')) {
					$(this).attr('src', $(this).attr('src').replace('_on.', '_off.'));
			}
		});	
		
		//------------------------.over を透過
		$(".over") .hover(function(){
				 $(this).stop().animate({'opacity' : '0.6'}, 140); // マウスオーバーで透明度を30%にする
			},function(){
				 $(this).stop().animate({'opacity' : '1'}, 100); // マウスアウトで透明度を100%に戻す
		});

});
