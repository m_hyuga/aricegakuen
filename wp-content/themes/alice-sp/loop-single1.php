<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>
<div class="IndexInfo">
<div class="TitleBox">
<h1 class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/info_title.jpg" alt="アリス学園からのお知らせ" width="213" height="44" /></h1>
<div class="clear"></div>
</div>

<div class="InfoBox">



<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="EntryTitle">

					<div class="Date">[<?php the_time('Y.n.j'); ?>]
</div>

<h2><?php the_title(); ?></h2>
</div>

					<div class="entry-content">
						<?php the_content(); ?>
					
					</div><!-- .entry-content -->



					
				</div><!-- #post-## -->

			</div>

</div>	
				

<?php endwhile; // end of the loop. ?>