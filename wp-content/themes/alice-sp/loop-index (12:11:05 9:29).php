<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>




<div class="IndexConL">

<!--アリス学園からのお知らせ-->

<div class="IndexInfo">
<div class="TitleBox">
<h3 class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/info_title.jpg" alt="アリス学園からのお知らせ" width="213" height="44" /></h3>
<div class="right"><a href="<?php bloginfo('url'); ?>/?cat=4"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/info_list_button.jpg" alt="お知らせ一覧を見る" width="152" height="35" /></a></div>
<div class="clear"></div>
</div>

<div class="TextBox">

<ul>
<?php

global $post;

$args = array( 'numberposts' => 10,  'category' => 4 );

$myposts = get_posts( $args );

foreach( $myposts as $post ) :  setup_postdata($post); ?>


<li>
<div class="DateBox">

[<?php the_time('Y.n.j'); ?>]
</div>

<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

</li>


<?php endforeach; ?>
</ul>

</div>

</div>

<!--アリス学園の取り組み-->

<div class="IndexInfo">
<div class="TitleBox">
<h3 class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/app_title.jpg" alt="アリス学園の取り組み" width="213" height="40" /></h3>
<div class="right"><a href="<?php bloginfo('url'); ?>/?cat=3"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/list_button.jpg" alt="一覧を見る" width="152" height="36" /></a></div>
<div class="clear"></div>
</div>
<div class="TextBox"> 
<?php

global $post;

$args = array( 'numberposts' => 1,  'category' => 3 );

$myposts = get_posts( $args );

foreach( $myposts as $post ) :  setup_postdata($post); ?>
<div class="TextBoxL">
 <?php echo c2c_get_custom('画像', '<img width="223" alt="" src="http://alice-japan.net/gakuen/wp-content/uploads/', '" />'); ?>

</div>
<div class="TextBoxR">
<div class="FB14 MB10">
[<?php the_time('Y.n.j'); ?>] <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</div>
<?php the_content(); ?>

</div>
<div class="clear"></div>
<?php endforeach; ?>
</div>
</div>



</div><!--IndexConL-->