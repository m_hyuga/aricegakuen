<?php
/**
 * Template Name: top-page
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header('top'); ?>
	<div id="wrap">
		<div class="area_img"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_main.jpg" width="100%"></div><!-- .area_img -->
		<?php
/* Run the loop to output the posts.
 * If you want to overload this in a child theme then include a file
 * called loop-index.php and that will be used instead.
 */
 get_template_part( 'loop', 'index' );
?>

		<div class="area_fb">
			<div class="fb-page" data-href="https://www.facebook.com/Alice.International.College" data-width="83%" data-height="350" data-hide-cover="true" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Alice.International.College"><a href="https://www.facebook.com/Alice.International.College">アリス学園グループ</a></blockquote></div></div>
	</div>

	<p class="center"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_footer.png" alt="人、愛、信頼の街づくりを・・・ アリス学園グループ" width="100%"></p>
		
	</div><!-- #wrap -->
<?php get_footer('top'); ?>
