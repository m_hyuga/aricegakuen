$(function(){
			var obj = $('.slide_area .slider').bxSlider({ // 自動再生
					auto: true,
					infiniteLoop: true,
					responsive: true,
					speed: 700,
					mode: 'horizontal',
					controls: true,
					slideWidth: 1200, // 画像のサイズ
					pager: false,
					pause: 4000
			});
			
			$('.area_open').click(function() {
        window.location = $(this).find('a').attr('href');
        return false;
    });


});
$(function() {
	$(window).on('load', function(){
	
		// メニューの整形
		$('nav').prepend('<div class="menu_over"></div>');
		var title_content = $('header h1').clone(true);
		$('header nav div:not(.menu_over)').prepend(title_content);

		var h = $('body').height();
		//メニュー開閉周辺
		$('nav .menu_over').css('height',h);
		//メニュー開き
		$('#menu').click(function() {
			$('nav').fadeIn(200, 'jswing');
		});
		//メニュー閉じ
		$('#close').click(function() {
			$('nav').fadeOut(200, 'jswing');
		});
	

});
});
