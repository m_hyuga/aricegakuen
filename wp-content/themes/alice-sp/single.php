<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="Contents">

		<div class="IndexConL">
<?php if ( in_category('4') ) : ?>
<?php include (TEMPLATEPATH . '/loop-single1.php'); ?>
 
<?php elseif ( in_category('3') ) : ?>
 <?php include (TEMPLATEPATH . '/loop-single2.php'); ?>

			




	      
<?php else : ?>
		
        
        

<h5 class="center">お探しのページは見つかりませんでした。</h5>


<?php endif; ?>
		</div><!-- .IndexConL -->
<br /><?php get_sidebar(); ?>
<?php get_footer(); ?>
