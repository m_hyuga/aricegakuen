<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	<div class="area_content cf">
	<section class="area_news">
		<h2><img src="<?php bloginfo( 'template_url' ); ?>/images/top/ttl_news.png" alt="新着情報" width="100%"></h2>
		<ul>
			<?php
			global $post;
			$args = array( 'numberposts' => 3,  'category' => 4 );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :  setup_postdata($post); ?>
			<li><a href="<?php the_permalink(); ?>"><span><?php the_time('Y.n.j'); ?></span><?php the_title(); ?></a></li>
			<?php endforeach; ?>
		</ul>
		<p class="btn_area"><a href="http://alice-japan.net/gakuen/?cat=4">一覧を見る</a></p>
	</section>
	<p class="area_bnr"><a href="http://alice-japan.net/gakuen/entrance/scholarship.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/bnr_shogakukin.jpg" alt="奨学金新設！" width="100%"></a></p>
	</div>
	<div class="slide_area">
	<ul class="slider">
		<li><a href="http://alice-japan.net/gakuen/subject/hoiku/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/bnr_fukushi.png" alt="福祉保育学科ご案内" width="100%"></a></li>
		<li><a href="http://alice-japan.net/gakuen/subject/kaigo/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/bnr_kaigo.png" alt="介護福祉学科ご案内" width="100%"></a></li>
	</ul>
	</div>
	<div class="area_open"><?php while (have_posts()) : the_post(); ?><a href="http://alice-japan.net/gakuen/other/opencampus.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/ttl_opencampus.png" alt="オープンキャンパス" width="100%" class="ttl"><span class="open_num"><?php $opencampus = post_custom('opencampus'); echo $opencampus; ?></span><span class="open_date"><?php $open_mon = post_custom('open_mon'); echo $open_mon; ?></span><span class="open_day">月</span><span class="open_date"><?php $open_day = post_custom('open_day'); echo $open_day; ?></span><span class="open_day">日</span><span class="open_week">[<?php $open_week = post_custom('open_week'); echo $open_week; ?>]</span><br><span class="open_time"><?php $open_time = post_custom('open_time'); echo $open_time; ?></span><br><img src="<?php bloginfo( 'template_url' ); ?>/images/top/bnr_opencampus_btm.png" alt="オープンキャンパス" width="100%"></a><?php endwhile; ?></div>
	<p class="area_shiryo"><a href="http://alice-japan.net/gakuen/other/request.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/bnr_shiryo.jpg" alt="資料請求 資料を申し込む" width="100%"></a></p>
	<div class="area_content cf">
	<section class="area_torikumi">
	<h2><img src="<?php bloginfo( 'template_url' ); ?>/images/top/ttl_torikumi.png" alt="アリス学園の取り組み" width="100%"></h2>
		<?php
		global $post;
		$args = array( 'numberposts' => 1,  'category' => 3 );
		$myposts = get_posts( $args );
		foreach( $myposts as $post ) :  setup_postdata($post); 
		?>
		<dl class="cf">
			<dt><a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" width="100%"></a></dt>
			<dd>
			<h3><a href="<?php the_permalink(); ?>"><span><?php the_time('Y.n.j'); ?></span><br><?php the_title(); ?></a></h3>
			<p><?php
					$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 60);
					$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
					$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
					echo $theContentForPreSingle;
					if(mb_strlen($theContentForPreSingle) >= 60){echo '...';};
					?></p>
			</dd>
		</dl>
		<p class="btn_area"><a href="<?php the_permalink(); ?>">詳しく見る</a></p>
		<?php endforeach; ?>
	</section>
	</div><!-- .area_content -->
