<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
	<meta name="format-detection" content="telephone=no" />
	<title>学校法人 アリス国際学園 専門学校アリス学園┃学校法人アリス国際学園</title>
	<link rel="stylesheet" href="http://www.css-reset.com/css/meyer.css" />
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/top.css" />
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/js/jquery.bxslider/jquery.bxslider.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/common.js"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.bxslider/jquery.bxslider.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
<?php	wp_head();?>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.3&appId=774065322650571";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<header class="cf">
		<h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/logo.jpg" alt="学校法人アリス国際学園　専門学校 アリス学園" width="100%"></a></h1>
		<p class="btn_menu"><a id="menu" href="javascript:boid(0);"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btn_menu.jpg" alt="" width="100%"></a></p>
		<nav class="clear">
			<div class="cf"><p  id="close"><a href="javascript:boid(0);"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btn_menu.jpg" alt="" width="100%"></a></p></div>
			<ul class="clear">
				<li><a href="http://alice-japan.net/gakuen/intro/index.html">学校案内</a></li>
				<li><a href="http://alice-japan.net/gakuen/subject/index.html">学科紹介</a></li>
				<li><a href="http://alice-japan.net/gakuen/entrance/index.html">募集要項</a></li>
				<li><a href="http://alice-japan.net/gakuen/other/opencampus.html">オープンキャンパス</a></li>
				<li><a href="http://alice-japan.net/gakuen/entrance/general.html">入試案内</a></li>
				<li><a href="http://alice-japan.net/gakuen/other/request.html">資料請求</a></li>
			</ul>
		</nav>
	</header>
