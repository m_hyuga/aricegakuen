<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="Description" content="専門学校アリス学園は福祉保育、介護福祉、日本語の3つ学科を就学することができます。" />
<meta name="Keywords" content="アリス学園,専門学校,福祉保育,介護福祉,日本語,日本文化教養学科" />
<title>学校法人 アリス国際学園 専門学校アリス学園┃学校法人アリス国際学園</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />

<link href="<?php bloginfo( 'template_url' ); ?>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php bloginfo( 'template_url' ); ?>/css/common.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php bloginfo( 'template_url' ); ?>/css/contents.css" rel="stylesheet" type="text/css" media="all" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src='<?php bloginfo( 'template_url' ); ?>/js/iepngfix.js' type="text/javascript"></script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27753393-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Put the following javascript before the closing </head> tag. --><script>
(function() {
var cx = '013505621044589144567:iynjgutyk7m';
var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
'//www.google.com/cse/cse.js?cx=' + cx;
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
})();
</script>
</head>

<body <?php body_class(); ?>>
<div id="wrap">
<div id="wrapper" class="indexBG">
<div id="contener">
<div id="header">
<h1><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/logo.jpg" alt="学校法人 アリス国際学園 専門学校アリス学園" width="255" height="41" /></a></h1>

<div id="GlobalNav">
<ul>
<li><a href="<?php bloginfo('url'); ?>/other/recruit.html"><img src="http://alice-japan.net/gakuen/images/common/header/nav01.jpg" alt="採用情報" width="55" height="16" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/other/access.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/nav02.jpg" alt="アクセス" width="54" height="15" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/other/question.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/nav03.jpg" alt="よくある質問" width="76" height="15" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/other/request.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/header/nav04.jpg" alt="お問い合わせ" width="85" height="13" /></a></li>
<br clear="all" />
</ul>
</div>

<!-- 検索ボックス始まり--><div id="Search">
<div class="s-text">
<img src="http://alice-japan.net/gakuen/images/common/header/g_search_s.jpg" alt="サイト内検索" /> アリス学園グループサイト内検索
</div>
<!-- Googleカスタム検索用の検索窓部分 -->
<div id="search-box">
<form action="http://www.google.com/cse" id="cse-search-box">
<div>
<input type="hidden" name="cx" value="013505621044589144567:iynjgutyk7m" />
<input type="hidden" name="ie" value="UTF-8" />
<input type="text" name="q" />
<input type="submit" name="sa" value="検索" />
</div>
</form>
</div>
<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=ja"></script>

</div>
<!-- 検索ボックス終り終り-->

<br clear="all" />
</div><!---/header--->


<div id="nav">
<ul>
<li><a href="<?php bloginfo('url'); ?>/intro/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav01.png" alt="学校案内" width="127" height="42" class="iepngfix" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/campus/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav02.png" alt="キャンパスライフ" width="129" height="42" class="iepngfix" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/subject/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav03.png" alt="学科紹介" width="129" height="42" class="iepngfix" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/entrance/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav04.png" alt="募集要項" width="129" height="42" class="iepngfix" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/recruit/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav05.png" alt="就職について" width="129" height="42" class="iepngfix" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/study/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav06.png" alt="生涯学習" width="128" height="42" class="iepngfix" /></a></li>
<li><a href="<?php bloginfo('url'); ?>/guidance/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/nav/nav07.png" alt="ガイダンスセミナー" width="129" height="42" class="iepngfix" /></a></li>
<div class="clear"></div>
</ul>
</div><!---/navi--->


