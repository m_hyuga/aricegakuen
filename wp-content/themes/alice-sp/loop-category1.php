<?php
/**
 * The loop that displays a category post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-category1.php.
 *

 */
?>

<div class="IndexInfo">
<div class="TitleBox">
<h3 class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/info_title.jpg" alt="アリス学園からのお知らせ" width="213" height="44" /></h3>
<div class="clear"></div>
</div>

<div class="TextBox">
<ul>
<?php
$posts = get_posts('posts_per_page=10&category=4&paged='.$paged);
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>


<li>
<div class="DateBox">

[<?php the_time('Y.n.j'); ?>]
</div>

<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br />


</li>


<?php endforeach; endif;?> 
</ul>
</div>

</div>
