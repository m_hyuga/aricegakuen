<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>
<div class="IndexInfo">
<div class="TitleBox">
<h3 class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/app_title.jpg" alt="アリス学園の取り組み" width="213" height="40" /></h3><div class="clear"></div>
</div>
<div class="InfoBox"> 
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                
                <div class="EntryL">
<p> <?php echo c2c_get_custom('画像', '<img width="223" alt="" src="http://alice-japan.net/gakuen/wp-content/uploads/', '" />'); ?></p><br />
<p><?php echo c2c_get_custom('画像2', '<img width="223" alt="" src="http://alice-japan.net/gakuen/wp-content/uploads/', '" />'); ?></p><br />
<p> <?php echo c2c_get_custom('画像3', '<img width="223" alt="" src="http://alice-japan.net/gakuen/wp-content/uploads/', '" />'); ?></p>
</div>

				<div class="EntryR">	
                   <div class="Date">[<?php the_time('Y.n.j'); ?>]</div> 
                    <h2><?php the_title(); ?></h2>
                    <br />
                    <div class="entry-content">
						<?php the_content(); ?>
					
					</div>
               </div>
               <div class="clear"></div>

					<!-- .entry-content -->



				
				</div><!-- #post-## -->

				
			

<?php endwhile; // end of the loop. ?>

</div>
</div>



