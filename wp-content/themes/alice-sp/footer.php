<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<div class="clear"></div>
</div>
</div>


</div>
<div id="footerWrapper">
<div id="footer">
<h1><a href="http://alice-japan.net/" title="アリス学園グループサイトトップページへ"><img src="<?php bloginfo( 'template_url' ); ?>/images/common/footer/footerlogo.jpg" alt="アリス学園グループ" width="311" height="105" class="left" /></a></h1>
<div class="Address">
<h2><img src="<?php bloginfo( 'template_url' ); ?>/images/common/footer/text01.jpg" alt="専門学校アリス学園" width="109" height="16" /></h2>
〒921-8176　金沢市円光寺本町8番50号<br />
TEL.076-280-1001/FAX.076-280-1002/MAIL.<a href="mailto:info@alice-japan.net">info@alice-japan.net</a>

</div>
<br clear="all" />


</div>
</div>
</div>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
