<?php
/**
 * The loop that displays a category post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-category2.php.
 *

 */
?>

<div class="IndexInfo">
<div class="TitleBox">
<h3 class="left"><img src="<?php bloginfo( 'template_url' ); ?>/images/index/app_title.jpg" alt="アリス学園の取り組み" width="213" height="40" /></h3>
<div class="right"></div>
<div class="clear"></div>
</div>
<div class="TextBox"> 
<ul>
<?php
$page = get_query_var('paged');
$posts = get_posts('numberposts=10&category=3');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<div class="FB14 MB10">
[<?php the_time('Y.n.j'); ?>] <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</div>

</li>

<?php endforeach; endif;?> 
</ul>
</div>


</div>
