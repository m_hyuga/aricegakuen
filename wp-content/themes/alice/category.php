<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<div id="Contents">

		<div class="IndexConL">

				
 <?php if ( is_category('4') ) : ?>
<?php include (TEMPLATEPATH . '/loop-category1.php'); ?>

<?php elseif ( is_category('3') ) : ?>
<?php include (TEMPLATEPATH . '/loop-category2.php'); ?>




	      
<?php else : ?>
		
        
        

<h1 class="center">お探しのページは見つかりませんでした。</h1>


<?php endif; ?>
<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div><!-- .IndexConL -->

<?php get_sidebar(other); ?>
<?php get_footer(); ?>
