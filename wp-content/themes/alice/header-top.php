<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="Description" content="専門学校アリス学園は福祉保育、介護福祉、日本語の3つ学科を就学することができます。" />
<meta name="Keywords" content="アリス学園,専門学校,福祉保育,介護福祉,日本語,日本文化教養学科" />
<title>学校法人 アリス国際学園 専門学校アリス学園┃学校法人アリス国際学園</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" href="http://www.css-reset.com/css/meyer.css" />
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/top.css" />
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/js/jquery.bxslider/jquery.bxslider.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/common.js"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.bxslider/jquery.bxslider.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27753393-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Put the following javascript before the closing </head> tag. --><script>
(function() {
var cx = '013505621044589144567:iynjgutyk7m';
var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
'//www.google.com/cse/cse.js?cx=' + cx;
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
})();
</script>

<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.3&appId=774065322650571";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<header>
		<h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/logo_header.jpg" alt="学校法人アリス国際学園　専門学校 アリス学園" class="over"></a></h1>
		<ul class="cf">
			<li><a href="http://alice-japan.net/gakuen/other/recruit.html">採用情報</a></li><!--
		--><li><a href="http://alice-japan.net/gakuen/other/access.html">アクセス</a></li><!--
		--><li><a href="http://alice-japan.net/gakuen/other/question.html">よくある質問</a></li><!--
		--><li><a href="http://alice-japan.net/gakuen/other/request.html">お問い合わせ</a></li>
		</ul>
		<nav>
			<ul class="cf">
				<li><a href="http://alice-japan.net/gakuen/intro/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/nav_annai_off.jpg" width="100" height="45" alt="学校案内"></a></li><!--
			--><li><a href="http://alice-japan.net/gakuen/campus/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/nav_campas_off.jpg" width="152" height="45" alt="キャンパスライフ"></a></li><!--
			--><li><a href="http://alice-japan.net/gakuen/subject/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/nav_gakka_off.jpg" width="100" height="45" alt="学科紹介"></a></li><!--
			--><li><a href="http://alice-japan.net/gakuen/entrance/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/nav_yoko_off.jpg" width="100" height="45" alt="募集要項"></a></li><!--
			--><li><a href="http://alice-japan.net/gakuen/entrance/general.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/nav_nyushi_off.jpg" width="100" height="45" alt="入試案内"></a></li><!--
			--><li><a href="http://alice-japan.net/gakuen/recruit/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/nav_shushoku_off.jpg" width="127" height="45" alt="就職について"></a></li><!--
			--><li><a href="http://alice-japan.net/gakuen/study/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/nav_shogai_off.jpg" width="100" height="45" alt="生涯学習"></a></li>
			</ul>
		</nav>
		<p><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_header.png" width="62" height="126"></p>
	</header>
