<?php
/**
 * Template Name: top-page
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header('top'); ?>
<div id="wrap">
<?php
/* Run the loop to output the posts.
 * If you want to overload this in a child theme then include a file
 * called loop-index.php and that will be used instead.
 */
 get_template_part( 'loop', 'index' );
?>

<p class="center"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_bottom.png" width="516" height="200" alt="人、愛、信頼の街づくりを・・・ アリス学園グループ"></p>

</div><!-- #wrap -->
<?php get_footer('top'); ?>
