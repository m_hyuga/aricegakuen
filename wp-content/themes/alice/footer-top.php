<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<footer class="cf">
	<h1><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/logo_footer.jpg" width="294" height="62" alt="アリス学園グループ" class="over"></a></h1>
</footer>
<?php wp_footer();?>
</body>
</html>
