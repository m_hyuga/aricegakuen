<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<div class="area_img">
	<ul class="slider">
		<?php while (have_posts()) : the_post(); 
			$val = wp_get_attachment_image_src(post_custom('slide_image'),'full' );
				if (empty($val)) {
					echo '';
				} else {
				echo '<img src="';
				echo $val[0];
				echo '" width="1200" height="566"></li>';
			}
			$val = wp_get_attachment_image_src(post_custom('slide_image02'),'full' );
				if (empty($val)) {
					echo '';
				} else {
				echo '<img src="';
				echo $val[0];
				echo '" width="1200" height="566"></li>';
			}
			$val = wp_get_attachment_image_src(post_custom('slide_image03'),'full' );
				if (empty($val)) {
					echo '';
				} else {
				echo '<img src="';
				echo $val[0];
				echo '" width="1200" height="566"></li>';
			}
			$val = wp_get_attachment_image_src(post_custom('slide_image04'),'full' );
				if (empty($val)) {
					echo '';
				} else {
				echo '<img src="';
				echo $val[0];
				echo '" width="1200" height="566"></li>';
			}
			$val = wp_get_attachment_image_src(post_custom('slide_image05'),'full' );
				if (empty($val)) {
					echo '';
				} else {
				echo '<img src="';
				echo $val[0];
				echo '" width="1200" height="566"></li>';
			}
		endwhile; ?>
	</ul>
</div><!-- .area_img -->
<div class="area_content cf">
	<section class="area_news">
		<h2><img src="<?php bloginfo( 'template_url' ); ?>/images/top/ttl_news.png" width="489" height="83" alt="新着情報"></h2>
		<p class="btn_area"><a href="http://alice-japan.net/gakuen/?cat=4"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btn_news.png" width="103" height="22" alt="一覧を見る" class="over"></a></p>
		<ul>
			<?php
			global $post;
			$args = array( 'numberposts' => 3,  'category' => 4 );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :  setup_postdata($post); ?>
			<li><a href="<?php the_permalink(); ?>"><span><?php the_time('Y.n.j'); ?></span><?php the_title(); ?></a></li>
			<?php endforeach; ?>
		</ul>
	</section>
	<p class="area_bnr"><a href="http://alice-japan.net/gakuen/entrance/scholarship.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btr_shogakukin.jpg" width="491" height="180" alt="アリス学園 特別奨学金新設！" class="over"></a></p>
	<div class="area_bnrs clear cf">
		<p><a href="http://alice-japan.net/gakuen/subject/hoiku/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btn_hoiku.jpg" width="359" height="422" alt="福祉保育学科ご案内" class="over"></a></p>
		<p><a href="http://alice-japan.net/gakuen/subject/kaigo/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btn_kaigo.jpg" width="358" height="422" alt="介護福祉学科ご案内" class="over"></a></p>
		
		<p class="open"><?php while (have_posts()) : the_post(); ?><a href="http://alice-japan.net/gakuen/other/opencampus.html" class="over"><span class="open_num"><?php $opencampus = post_custom('opencampus'); echo $opencampus; ?></span><span class="open_date"><?php $open_mon = post_custom('open_mon'); echo $open_mon; ?></span><span class="open_day">月</span><span class="open_date"><?php $open_day = post_custom('open_day'); echo $open_day; ?></span><span class="open_day">日</span><span class="open_week">[<?php $open_week = post_custom('open_week'); echo $open_week; ?>]</span><br><span class="open_time"><?php $open_time = post_custom('open_time'); echo $open_time; ?></span></a><?php endwhile; ?></p>
		
		<p><a href="http://alice-japan.net/gakuen/other/request.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btn_shiryo.jpg" width="288" height="160" alt="資料請求 資料を申し込む" class="over"></a></p>
	</div>
</div><!-- .area_content -->
<br>
<br>
<section class="area_campus">
	<h2><a href="http://alice-japan.net/gakuen/campus/index.html"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/ttl_campaslife.png" width="314" height="32" alt="キャンパスライフ" class="over"></a></h2>
	<ul class="slider">
		<li><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_01.jpg" width="212" height="159"></li>
		<li><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_02.jpg" width="212" height="159"></li>
		<li><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_03.jpg" width="212" height="159"></li>
		<li><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_04.jpg" width="212" height="159"></li>
		<li><img src="<?php bloginfo( 'template_url' ); ?>/images/top/img_05.jpg" width="212" height="159"></li>
	</ul>
</section><!-- .area_campus -->

<div class="area_bottom cf">
	<section class="area_torikumi fll">
		<h2><img src="<?php bloginfo( 'template_url' ); ?>/images/top/ttl_torikumi.png" width="433" height="33" alt="アリス学園の取り組み"></h2>
		<?php
		global $post;
		$args = array( 'numberposts' => 1,  'category' => 3 );
		$myposts = get_posts( $args );
		foreach( $myposts as $post ) :  setup_postdata($post); 
		?>
		<dl class="cf">
			<dt><a href="<?php the_permalink(); ?>" class="over"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"></a></dt>
			<dd>
			<h3><a href="<?php the_permalink(); ?>"><?php the_time('Y.n.j'); ?> <?php the_title(); ?></a></h3>
			<p><?php
					$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 130);
					$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
					$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
					echo $theContentForPreSingle;
					if(mb_strlen($theContentForPreSingle) >= 130){echo '...';};
					?></p>
			</dd>
		</dl>
		<p class="btn_area"><a href="<?php the_permalink(); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/top/btn_more.png" width="346" height="27" alt="詳しく見る" class="over"></a></p>
		<?php endforeach; ?>
	</section>
	<div class="area_fb">
	<div class="fb-page" data-href="https://www.facebook.com/Alice.International.College" data-width="520" data-height="385" data-hide-cover="true" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Alice.International.College"><a href="https://www.facebook.com/Alice.International.College">アリス学園グループ</a></blockquote></div></div>
	</div>
</div><!-- .area_bottom -->
